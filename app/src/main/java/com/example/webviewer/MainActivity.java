package com.example.webviewer;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

public class MainActivity extends AppCompatActivity {

    DBHelper dbHelper; //Класс для работы с SQLite

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView Wrong = (TextView)findViewById(R.id.textWrong);  //Сообщение об ошибке
        Wrong.setVisibility(View.INVISIBLE);

        dbHelper = new DBHelper(this);

        //Обработка нажатия кнопки
        Button buttonLogin = (Button)findViewById(R.id.buttonLogin); //Кнопка входа
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SQLiteDatabase database = dbHelper.getWritableDatabase();

                final EditText editLog = (EditText)findViewById(R.id.editLogin);    //Поле ввода логина
                final EditText editPass = (EditText)findViewById(R.id.editPassword);//Поле ввода пароля
                final String login = editLog.getText().toString(); //Текст из поля логина
                final String pass = editPass.getText().toString(); //Текст из поля пароля

                //Инициализация курсора бд
                Cursor cursor = database.query(DBHelper.TB_USER, null, null,
                                null, null, null, null);

                String url = null;    //Строка для записи адреса веб-сайта
                boolean flag = false; //Флаг для проверки входа
                //Проверка логина и пароля
                if (cursor.moveToFirst()) {
                    int loginIndex = cursor.getColumnIndex(DBHelper.KEY_LOGIN);
                    int passwordIndex = cursor.getColumnIndex(DBHelper.KEY_PASSWORD);
                    int urlIndex = cursor.getColumnIndex(DBHelper.KEY_URL);
                    do {
                        if(cursor.getString(loginIndex).equals(login) && cursor.getString(passwordIndex).equals(pass)){
                            flag = true;
                            url = cursor.getString(urlIndex); //Получаем из бд соответствующий url
                        }
                    } while (!flag && cursor.moveToNext());
                }

                cursor.close(); //Закрытие курсора

                if (flag) { //Если ввод коректный
                        //Переход на следующие Activity
                        Intent intent = new Intent(MainActivity.this, WebView.class);
                        //Передаём на следующие Activity данные о пользователе и сайте
                        intent.putExtra("user", login); //Имя пользователя
                        intent.putExtra("url", url);    //сайт пользователя
                        startActivity(intent); //Начинаем следущие Activity
                        finish(); //Заканчиваем это Activity
                }
                else{ //Если ввод не коректный
                    Wrong.setVisibility(View.VISIBLE); //Вывести сообщение о неправильном вводе
                }
            }
        });
        //Окно на весь экран
        Window w = getWindow();
        w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }
}
