package com.example.webviewer;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebBackForwardList;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class WebView extends AppCompatActivity {
    private android.webkit.WebView WView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);

        //Получаем переданные данные с прошлого Activity
        Bundle arguments = getIntent().getExtras();
        String user = arguments.get("user").toString(); //Имя пользователя
        String url = arguments.get("url").toString();   //Сайт пользователя

        //Добавление в шапку данных пользователя
        final TextView UserText = (TextView)findViewById(R.id.TextUser); //Поле имени пользователя
        UserText.setText("User: " + user); //Вставляем имя
        final TextView UrlText = (TextView)findViewById(R.id.TextUrl);   //Поле сайта пользователя
        UrlText.setText("Url: " + url);    //Вставляем сайт

        //WebView
        WView = (android.webkit.WebView) findViewById(R.id.webview); //Находим виджет
        WView.setWebViewClient(new WebViewClient());
        WView.loadUrl(url); //Загружаем сайт

        //Окно на весь экран
        Window w = getWindow();
        w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    //Обработка системной кнопки назад
    @Override
    public void onBackPressed() {
        if(WView.canGoBack()){ //Если есть предыдущие страницы webview
            WView.goBack();    //Назад по WebView
        } else {
            //Назад на начальный Activity
            Intent intent = new Intent(WebView.this, MainActivity.class);
            startActivity(intent); //Начинаем Activity
            finish();   //Закрываем это Activity
        }
    }
}
