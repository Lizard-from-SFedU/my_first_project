package com.example.webviewer;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DBHelper extends SQLiteOpenHelper {

    public static final int DB_VERSION = 3;         //Версия бд
    public static final String DB_NAME = "logins";  //Название бд
    public static final String TB_USER = "user";    //Название таблицы

    public static final String KEY_ID = "_id";           //id записи
    public static final String KEY_LOGIN = "login";      //Логин пользователя
    public static final String KEY_PASSWORD = "password";//Пароль пользователя
    public static final String KEY_URL = "url";          //Сайт

    public DBHelper(@Nullable Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + TB_USER + "(" + KEY_ID + " integer primary key," +
                KEY_LOGIN + " text," + KEY_PASSWORD + " text," + KEY_URL + " text)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + TB_USER);

        onCreate(db);
    }
}
